module.exports = (figureSize, canvas) => {
  const diameter = Math.sqrt(2 * figureSize * figureSize);
  canvas.setAttribute('height', diameter);
  canvas.setAttribute('width', diameter);

  if (canvas.getContext) {
    const ctx = canvas.getContext('2d');
    ctx.fillStyle = '#4CAF50';

    /* Draw rhombus */
    ctx.rotate((45 * Math.PI) / 180);
    ctx.fillRect(figureSize / 2, -(figureSize / 2), figureSize, figureSize);
    ctx.arc(figureSize, 0, figureSize / 3, 0, 2 * Math.PI, false);

    /* Draw circle */
    ctx.fillStyle = '#455a64';
    ctx.fill();
  }
};
