![](readme.png)


>This is node starter template with code autofix and webpack builder.

Code autofixing feature runs on webpack server-side, so you do not need to configure linting settings in your IDE.

Autofixing feature enabled for all `.js`, `.css`, `.html` files stored in `./src` and configures via dotenv `.env` file

# Installed packages

> Just info about used dependencies

All used packages:

```
npm install dotenv --save-dev
npm install css-minimizer-webpack-plugin --save-dev
npm install style-loader css-loader sass-loader sass --save-dev
npm install clean-webpack-plugin clean-webpack-plugin mini-css-extract-plugin html-webpack-plugin workbox-webpack-plugin babel-plugin-syntax-dynamic-import --save-dev
npm install eslint prettier eslint-config-airbnb-base --save-dev
npm install eslint-config-prettier eslint-plugin-prettier eslint-webpack-plugin --save-dev
npm install babel-loader @babel/core @babel/cli @babel/eslint-parser @babel/preset-env --save-dev
npm install babel-eslint --save-dev
npm install webpack webpack-cli webpack-dev-server --save-dev
npm install stylelint stylelint-webpack-plugin --save-dev
npm install stylelint-prettier stylelint-config-prettier --save-dev
```

# Project initiation

> Project fast start procedure


Create project directory

```
mkdir /path/to/NewProject
cd /path/to/NewProject
```

Clone this repo
```
git clone https://gitlab.com/deSoul/node-starter .
```

Install packages
```
npm install
```

Run and fun!

# Project run

> Run project in various modes

Build project and run main js bundle by Node
```
npm run start
```

Serve project from `./src` to webserver on `localhost:8080` with hot-reload on file changes
```
npm run serve
```

Build project only
```
npm run build
```

Develop mode with code autofix on file changes
```
npm run watch
```

