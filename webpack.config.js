require('dotenv').config()
const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    /* Webpack working mode */
    mode: process.env.MODE,
    /* Webpack compile inputs */
    entry: path.join(__dirname, 'src/index.js'),
    /* Webpack compile outputs */
    output: {
        path: path.resolve(__dirname, "build"),
        filename: '[name].bundle.js',
        clean: true,
    },
    /* Webpack development server */
    devServer: {
        static: {
            directory: path.join(__dirname, 'build'),
        },
        compress: true,
        open: true,
        hot: true,
        host: process.env.HOST,
        port: process.env.PORT,
    },
    /* Webpack plugins */
    plugins: [
        /* HTML template generator */
        new HtmlWebpackPlugin({
            title: process.env.APPNAME,
            favicon: "src/favicon.png",
            template: 'src/index.html',
        }),
        /* Linter for JavaScript */
        new ESLintPlugin({
            eslintPath: 'eslint',
            context: 'src',
            files: '*.js',
            fix: Boolean(process.env.ESLINT_AUTOFIX),
            quiet: false,
            extensions: ['js', 'ts', 'tsx'],
        }),
        /* Linter for CSS */
        new StylelintPlugin({
            configFile: '.stylelintrc',
            context: 'src',
            files: '*.css',
            fix: Boolean(process.env.STYLEINT_AUTOFIX),
            failOnError: false
        }),
        /* CSS extractor */
        new MiniCssExtractPlugin({
            filename: '[name].style.css',
        }),
        new CleanWebpackPlugin(),
    ],
    /* Webpack modules */
    module: {
        rules: [
            /* JavaScript loaders */
            {
                loader: 'babel-loader',
                test: /\.(js|jsx)$/,
                include: [path.resolve(__dirname, 'src')]
            },
            /* Style loaders */
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader , "css-loader"],
            },
            /* Media loaders */
            {
                test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
                type: 'asset/resource',
            }
        ]
    },
    /*  CSS minification */
    optimization: {
        minimize: true,
        minimizer: [
            `...`,
            new CssMinimizerPlugin(),
        ],
    },
};
